using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Services;


namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host
{
    public class ImportWorker : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _serviceProvider;

        public ImportWorker(ILogger<ImportWorker> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }
        private async Task ImportData()
        {
            _logger.LogInformation("Import Background Service работает.");

            using var scope = _serviceProvider.CreateScope();
            var importService = 
                scope.ServiceProvider
                    .GetRequiredService<ICreditApplicationImportService>();

            await importService.ImportAsync();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return ImportData();
        }
    }
}