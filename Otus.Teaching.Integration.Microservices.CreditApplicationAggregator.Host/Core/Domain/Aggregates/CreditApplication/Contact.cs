﻿﻿using System;

 namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication
{
    public class Contact
    {

        public Guid Id { get; set; }
        
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }
    }
}