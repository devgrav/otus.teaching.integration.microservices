﻿using System.Threading.Tasks;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Services
{
    public interface ICreditApplicationImportService
    {
        Task ImportAsync();
    }
}