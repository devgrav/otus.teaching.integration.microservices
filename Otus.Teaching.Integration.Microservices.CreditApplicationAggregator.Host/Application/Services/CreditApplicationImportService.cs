﻿using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Services;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Application.Services
{
    public class CreditApplicationImportService
        : ICreditApplicationImportService
    {
        public Task ImportAsync()
        {
            //Далее вызываем методы, которые позволяют получить файл из сетевого хранилища, как при экспорте
            //А потом выполняем загрузку в БД, например, как делали в ДЗ по импорту данных в многопоточном режиме
            
            return Task.CompletedTask;
        }
    }
}