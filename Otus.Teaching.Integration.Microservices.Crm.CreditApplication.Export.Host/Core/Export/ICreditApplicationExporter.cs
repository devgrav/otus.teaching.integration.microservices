﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Export
{
    public interface ICreditApplicationExporter
    {
        Task ExportAsync(List<CreditApplicationExportDto> applications);
    }
}