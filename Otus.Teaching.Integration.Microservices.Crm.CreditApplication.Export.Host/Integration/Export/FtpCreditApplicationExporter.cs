﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Export;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Integration.Export
{
    public class FtpCreditApplicationExporter
        : ICreditApplicationExporter
    {
        private readonly ICreditApplicationFileSerializer _creditApplicationFileSerializer;
        private readonly FtpExportSettings _ftpExportSettings;
        
        public FtpCreditApplicationExporter(IOptions<FtpExportSettings> ftpExportSettings, ICreditApplicationFileSerializer creditApplicationFileSerializer)
        {
            _creditApplicationFileSerializer = creditApplicationFileSerializer;
            _ftpExportSettings = ftpExportSettings.Value;
        }
        
        public async Task ExportAsync(List<CreditApplicationExportDto> applications)
        {
            _creditApplicationFileSerializer.SerializeToFile(applications);
            
            if (!File.Exists(_ftpExportSettings.FileName))
                throw new FileNotFoundException("Файл для экспорта не был сформирован");
            
            // Создаем запрос для отправки на сервер
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(_ftpExportSettings.FtpUrl);
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // Логинимся
            request.Credentials = new NetworkCredential(_ftpExportSettings.FtpUser, _ftpExportSettings.FtpPassword);

            // Читаем данные файла
            byte[] fileContents;
            using (StreamReader sourceStream = new StreamReader(_ftpExportSettings.FileName))
            {
                fileContents = Encoding.UTF8.GetBytes(await sourceStream.ReadToEndAsync());
            }

            request.ContentLength = fileContents.Length;

            // Копируем данные в поток запроса
            await using (Stream requestStream = await request.GetRequestStreamAsync())
            {
                requestStream.Write(fileContents, 0, fileContents.Length);
            }

            using FtpWebResponse response = (FtpWebResponse)(await request.GetResponseAsync());
            
            Console.WriteLine($"Загрузка файла завершена, status {response.StatusDescription}");
        }
    }
}