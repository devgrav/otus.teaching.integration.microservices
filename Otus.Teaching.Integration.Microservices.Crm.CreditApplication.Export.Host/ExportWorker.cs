using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Export;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Repositories;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export
{
    public class ExportWorker : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _serviceProvider;
        private Timer _timer;

        public ExportWorker(ILogger<ExportWorker> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Export Background Service запускается.");

            _timer = new Timer(ExportData, cancellationToken, TimeSpan.Zero, 
                TimeSpan.FromSeconds(30));

            return Task.CompletedTask;
        }

        private async void ExportData(object state)
        {
            _logger.LogInformation("Export Background Service работает.");

            using var scope = _serviceProvider.CreateScope();
            var repository = 
                scope.ServiceProvider
                    .GetRequiredService<ICreditApplicationRepository>();
            var exporter = 
                scope.ServiceProvider
                    .GetRequiredService<ICreditApplicationExporter>();

            var applications= await repository.GetAllForExportAsync();
            
            await exporter.ExportAsync(applications);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Export Background Service останавливается.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}