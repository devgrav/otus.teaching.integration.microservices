﻿using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.Contract;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host.Integration
{
    public interface ICbsIntegrationGateway
    {
        Task CreateNewCreditApplicationAsync(CreateNewCreditApplicationContract application);
    }
}