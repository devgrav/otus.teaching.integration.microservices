using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Integration.Microservices.Contract;
using Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host.Consumers;
using GenericHost = Microsoft.Extensions.Hosting.Host;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host
{
    public class Program
    {
        public static async Task Main()
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var integrationSettings = new IntegrationEndpointSettings();
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                IConfigurationRoot configuration = builder.Build();

                configuration.GetSection("IntegrationEndpoints").Bind(integrationSettings);

                cfg.Host(integrationSettings.MessageBrokerEndpoint);

                cfg.ReceiveEndpoint("createNewCreditApplication", e =>
                {
                    e.Consumer<CreateNewCreditApplicationConsumer>();
                });
            });
            
            Console.WriteLine("Ожидаем сообщения...");
            await busControl.StartAsync();
        }
    }
}