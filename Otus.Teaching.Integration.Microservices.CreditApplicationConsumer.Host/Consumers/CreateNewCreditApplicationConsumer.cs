﻿﻿using System;
using System.Threading.Tasks;
 using MassTransit;
 using Otus.Teaching.Integration.Microservices.Contract;
 using Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host.Integration;

 namespace Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host.Consumers
{
    public class CreateNewCreditApplicationConsumer :
            IConsumer<CreateNewCreditApplicationContract>
        {
            private readonly ICbsIntegrationGateway _cbsIntegrationGateway;

            public CreateNewCreditApplicationConsumer()
            {
                _cbsIntegrationGateway = new CbsIntegrationGateway();
            }
            
            public async Task Consume(ConsumeContext<CreateNewCreditApplicationContract> context)
            {
                var application = context.Message;
             
                Console.WriteLine($"Получили команду на создание кредитной заявки с Id {application.Id} из шины на обработку...");
                
                if(application == null)
                    throw new ArgumentNullException(nameof(application));

                await _cbsIntegrationGateway.CreateNewCreditApplicationAsync(context.Message);
                
                Console.WriteLine($"Обработали команду на создание кредитной заявки с Id {application.Id} из шины");
            }
        }
    
}