﻿using System;

namespace Otus.Teaching.Integration.Microservices.Contract
{
    public interface CreateNewCreditApplicationContract
    {
        Guid Id { get; set; }
        
        Guid CustomerId { get; set; }
        
        string Channel { get; set; }
        
        DateTime CreatedDate { get; set; }
        
        string Status { get; set; }
        
        decimal Sum { get; set; }
    }
}