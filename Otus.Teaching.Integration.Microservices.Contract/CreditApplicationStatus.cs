﻿namespace Otus.Teaching.Integration.Microservices.Contract
{
    public enum CreditApplicationStatus
    {
        New = 1,
        InWork = 2,
        Finished = 3
    }
}